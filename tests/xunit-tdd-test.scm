(define-module (tests test-case-test)
  #:use-module ((rnrs) #:version (6) #:select (assert))
  #:use-module ((xunit-tdd test-case) #:prefix test-case:)
  #:use-module ((xunit-tdd was-run) #:prefix was-run:)
  #:use-module ((xunit-tdd test-result) #:prefix test-result:)
  #:use-module ((xunit-tdd test-suite) #:prefix test-suite:))

(let ([a-test-result #f])
  
  (define (setup _)
    (set! a-test-result (test-result:new)))

  (define (test-case-test:new test-proc)
    (test-case:new setup test-proc #f ""))
  

  (define (test-template-method this-test-case)
    (let ([a-test-case (was-run:new was-run:test-procedure)])
      (test-case:run a-test-case a-test-result)
      (assert (string=? "test-setup test-procedure test-teardown "
			(test-case:read-log a-test-case)))))

  (define (test-result this-test-case)
    (let ([a-test-case (was-run:new was-run:test-procedure)])
      (test-case:run a-test-case a-test-result)
      (assert (string=? "1 run, 0 failed"
			(test-result:summary a-test-result)))))

  (define (test-failed-result this-test-case)
    (let ([a-test-case (was-run:new was-run:test-broken-procedure)])
      (test-case:run a-test-case a-test-result)
      (assert (string=? "1 run, 1 failed"
			(test-result:summary a-test-result)))))

  (define (test-failed-result-formatting this-test-case)
    (test-result:test-started a-test-result)
    (test-result:test-failed a-test-result)
    (assert (string=? "1 run, 1 failed"
		      (test-result:summary a-test-result))))

  (define (test-suite this-test-case)
    (let ([a-test-suite (test-suite:new)])
      (test-suite:add a-test-suite (was-run:new was-run:test-procedure))
      (test-suite:add a-test-suite (was-run:new was-run:test-broken-procedure))
      (test-suite:run a-test-suite a-test-result)
      (assert (string=? "2 run, 1 failed"
			(test-result:summary a-test-result)))))

  (let ([the-test-suite (test-suite:new)]
	[the-test-result (test-result:new)])
    
    (test-suite:add the-test-suite (test-case-test:new test-template-method))
    (test-suite:add the-test-suite (test-case-test:new test-result))
    (test-suite:add the-test-suite (test-case-test:new test-failed-result))
    (test-suite:add the-test-suite (test-case-test:new test-failed-result-formatting))
    (test-suite:add the-test-suite (test-case-test:new test-suite))

    (test-suite:run the-test-suite the-test-result)
    (test-result:summary the-test-result)))
