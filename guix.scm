(use-modules
 (gnu packages guile)
 (gnu packages code)
 (guix packages)
 (guix build-system guile)
 (guix build utils)
 (guix licenses)
 (guix git-download)
 (guix gexp)
 (ice-9 popen)
 (ice-9 rdelim))

(define source-dir (dirname (current-filename)))
(define git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f 2" OPEN_READ)))
(define revision "0")
(define hash "")

(package
 (name "guile-xunit-tdd")
 (version (string-append (git-version "0.1.0" revision git-commit)"-HEAD"))
 (build-system guile-build-system)
 (source (local-file source-dir
		     #:recursive? #t
		     ;;#:select? (not (basename ".git"))
		     ))
 (native-inputs `(("guile" ,guile-3.0-latest)))
 (arguments
  '(#:phases
    (modify-phases %standard-phases
		   (add-after 'unpack 'move-src-files
			      (lambda (delete-file "guix.scm") #t)))))
 (synopsis "Unit Testing Framework for the Guile Programming Language")
 (description "This framework development follows the example provided by Kent Beck in his book « Test Driven Development by Example » Part2." )
 (license gpl3+)
 (home-page "https://jeko.frama.io"))
