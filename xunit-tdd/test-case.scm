(define-module (xunit-tdd test-case)
  #:use-module (srfi srfi-9)
  #:use-module ((xunit-tdd test-result) #:prefix test-result:))

(define-record-type <test-case>
  (make-test-case setup-proc test-proc teardown-proc log)
  test-case?
  (setup-proc setup-proc)
  (test-proc test-proc)
  (teardown-proc teardown-proc)
  (log log set-log!))

(define-public (new a-setup-proc a-test-proc a-teardown-proc a-log)
  (make-test-case a-setup-proc a-test-proc a-teardown-proc a-log))

(define-public (run this-test-case this-test-result)
  (test-result:test-started this-test-result)
  (for-each
   (lambda (proc)
     (with-exception-handler
	 (lambda (e)
	   (test-result:test-failed this-test-result))
       (lambda ()
	 (if (procedure? (proc this-test-case))
	     ((proc this-test-case) this-test-case)))
       #:unwind? #t))
   (list setup-proc test-proc teardown-proc)))

(define-public (read-log test-case)
  (log test-case))

(define-public (append-to-log! test-case a-log)
  (set-log! test-case (string-append (log test-case) a-log)))
