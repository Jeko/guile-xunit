(define-module (xunit-tdd was-run)
  #:use-module ((xunit-tdd test-case) #:prefix test-case:))

(define-public (new proc)
  (test-case:new test-setup proc test-teardown ""))

(define (test-setup test-case)
  (test-case:append-to-log! test-case "test-setup "))

(define (test-teardown test-case)
  (test-case:append-to-log! test-case "test-teardown "))

(define-public (test-procedure test-case)
  (test-case:append-to-log! test-case "test-procedure "))

(define-public (test-broken-procedure test-case)
  (raise-exception (make-exception)))
