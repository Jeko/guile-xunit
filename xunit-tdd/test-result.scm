(define-module (xunit-tdd test-result)
  #:use-module (srfi srfi-9))

(define-record-type <test-result>
  (make-test-result run fail)
  test-result?
  (run get-run set-run!)
  (fail get-fail set-fail!))

(define-public (new)
  (make-test-result 0 0))

(define-public (summary a-test-result)
  (format #f "~A run, ~A failed" (get-run a-test-result) (get-fail a-test-result)))

(define-public (test-started a-test-result)
  (set-run! a-test-result (+ 1 (get-run a-test-result))))

(define-public (test-failed a-test-result)
  (set-fail! a-test-result (+ 1 (get-fail a-test-result))))
