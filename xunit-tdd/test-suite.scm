(define-module (xunit-tdd test-suite)
  #:use-module (srfi srfi-9)
  #:use-module ((xunit-tdd test-case) #:prefix test-case:))

(define-record-type <test-suite>
  (make-test-suite tests)
  test-suite?
  (tests tests set-tests!))

(define-public (new)
  (make-test-suite '()))

(define-public (add test-suite test-case)
  (set-tests! test-suite (cons test-case (tests test-suite))))

(define-public (run test-suite test-result)
  (for-each 
   (lambda (test)
     (test-case:run test test-result))
   (tests test-suite)))
